
exports.getColorCode = (color_name) => {
    switch (color_name) {
        case "transparent":
            return "transparent";
            break;
        case "Black":
            return "#212121";
            break;
        case "Blue":
            return "#0057A6";
            break;
        case "Bright Green":
            return "#10CB31";
            break;
        case "Bright Light Blue":
            return "#9FC3E9";
            break;
        case "Bright Light Orange":
            return "#F7BA30";
            break;
        case "Bright Light Yellow":
            return "#F3E055";
            break;
        case "Bright Pink":
            return "#FFBBFF";
            break;
        case "Brown":
            return "#3399FF";
            break;
        case "Dark Azure":
            return "#3399FF";
            break;
        case "Dark Blue":
            return "#143044";
            break;
        case "Dark Bluish Gray":
            return "#595D60";
            break;
        case "Dark Gray":
            return "#6B5A5A";
            break;
        case "Dark Green":
            return "#2E5543";
            break;
        case "Dark Orange":
            return "#B35408";
            break;
        case "Dark Pink":
            return "#C87080";
            break;
        case "Dark Purple":
            return "#5F2683";
            break;
        case "Dark Red":
            return "#6A0E15";
            break;
        case "Dark Tan":
            return "#907450";
            break;
        case "Dark Turquoise":
            return "#008A80";
            break;
        case "Green":
            return "#00642E";
            break;
        case "Lavender":
            return "#B18CBF";
            break;
        case "Light Aqua":
            return "#CCFFFF";
            break;
        case "Light Blue":
            return "#B4D2E3";
            break;
        case "Light Bluish Gray":
            return "#AFB5C7";
            break;
        case "Light Gray":
            return "#9C9C9C";
            break;
        case "Light Lime":
            return "#EBEE8F";
            break;
        case "Light Nougat":
            return "#FECCB0";
            break;
        case "Light Pink":
            return "#FFE1FF";
            break;
        case "Light Purple":
            return "#DA70D6";
            break;
        case "Light Yellow":
            return "#FFE383";
            break;
        case "Lime":
            return "#A6CA55";
            break;
        case "Maersk Blue":
            return "#6BADD6";
            break;
        case "Magenta":
            return "#B52952";
            break;
        case "Medium Azure":
            return "#42C0FB";
            break;
        case "Medium Blue":
            return "#61AFFF";
            break;
        case "Medium Lavender":
            return "#885E9E";
            break;
        case "Medium Lime":
            return "#BDC618";
            break;
        case "Medium Nougat":
            return "#E3A05B";
            break;
        case "Medium Orange":
            return "#FFA531";
            break;
        case "Medium Violet":
            return "#9391E4";
            break;
        case "Nougat":
            return "#FFAF7D";
            break;
        case "Olive Green":
            return "#7C9051";
            break;
        case "Orange":
            return "#FF7E14";
            break;
        case "Pink":
            return "#FFC0CB";
            break;
        case "Purple":
            return "#A5499C";
            break;
        case "Red":
            return "#B30006";
            break;
        case "Reddish Brown":
            return "#89351D";
            break;
        case "Sand Blue":
            return "#5A7184";
            break;
        case "Sand Green":
            return "#76A290";
            break;
        case "Sand Red":
            return "#8C6B6B";
            break;
        case "Tan":
            return "#DEC69C";
            break;
        case "Very Light Orange":
            return "#E6C05D";
            break;
        case "White":
            return "#FFFFFF";
            break;
        case "Yellow":
            return "#F7D117";
            break;
        case "Yellowish Green":
            return "#DFEEA5";
            break;
        case "Trans-Clear":
            return "#EEEEEE";
            break;
        case "Trans-Dark Blue":
            return "#00296B";
            break;
        case "Trans-Light Blue":
            return "#68BCC5";
            break;
        case "Trans-Light Orange":
            return "#E99A3A";
            break;
        case "Trans-Neon Green":
            return "#C0F500";
            break;
        case "Trans-Neon Orange":
            return "#FF4231";
            break;
        case "Trans-Orange":
            return "#D04010";
            break;
        case "Trans-Yellow":
            return "#EBF72D";
            break;
        case "Chrome Gold":
            return "#F1F2E1";
            break;
        case "Chrome Silver":
            return "#DCDCDC";
            break;
        case "Pearl Light Gray":
            return "#ACB7C0";
            break;
        case "Metallic Gold":
            return "#B8860B";
            break;
        case "Milky White":
            return "#D4D3DD";
            break;
        case "Fabuland Brown":
            return "#b06d55";
            break;

    }

}